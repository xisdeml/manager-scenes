using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Manager_Scenes : GenericSingleton <Manager_Scenes>
{
    protected override bool DontDestroyOnLoad => true;

    private const string TRANSITION_BACKGROUND_PATH = "Blocker_SceneTransition";

    public enum SceneName {}

    private static AsyncOperation _changingOperation;

    private static CanvasGroup _transitionBackground;

    private const float _transitionFadeDuration = 1;

    private const float _minTransitionDuration = 1;


    private new void Awake()
    {
        base.Awake();

        _transitionBackground = Instantiate(Resources.Load<CanvasGroup>(TRANSITION_BACKGROUND_PATH));

        if (_transitionBackground == null)
            return;

        DontDestroyOnLoad(_transitionBackground);
        _transitionBackground.gameObject.SetActive(true);
        _transitionBackground.alpha = 0;
        _transitionBackground.interactable = false;
        _transitionBackground.blocksRaycasts = false;
    }

    public static void ChangeScene(SceneName scene, Action callback = null, float transitionDuration = _transitionFadeDuration)
    {
        if(_changingOperation != null)
        {
            Debug.LogError("Already have a transition");
            return;
        }

        Instance.StartCoroutine(Transition(scene, callback, transitionDuration));
    }

    public static void ChangeScene(SceneName scene, UnityEngine.Events.UnityEvent callback, float transitionDuration = _transitionFadeDuration)
    {
        if (callback != null)
            ChangeScene(scene, callback.Invoke, transitionDuration);
        
        else
            ChangeScene(scene, () => { }, transitionDuration);
    }

    private static IEnumerator Transition(SceneName scene, Action callback, float transitionDuration)
    {
        if(transitionDuration <= 0 || _transitionBackground == null)
        {
            SceneManager.LoadScene(scene.ToString());
            callback?.Invoke();
            yield break;
        }

        _changingOperation = SceneManager.LoadSceneAsync(scene.ToString());
        _changingOperation.allowSceneActivation = false;

        _transitionBackground.gameObject.SetActive(true);
        _transitionBackground.blocksRaycasts = true;

        float percent = 0;

        while(percent < 1)
        {
            _transitionBackground.alpha = percent;

            percent += Time.unscaledDeltaTime / transitionDuration;

            yield return null;
        }

        _transitionBackground.alpha = 1;

        float freezeTime = Time.unscaledTime + _minTransitionDuration;

        while (Time.unscaledTime < freezeTime)
        {
            yield return null;
        }

        while (_changingOperation.progress < 0.9f)
            yield return null;

        _changingOperation.allowSceneActivation = true;

        percent = 0;

        while (percent < 1)
        {
            _transitionBackground.alpha = 1 - percent;

            percent += Time.unscaledDeltaTime / transitionDuration;

            yield return null;
        }

        _transitionBackground.alpha = 0;

        _transitionBackground.blocksRaycasts = false;
        _transitionBackground.gameObject.SetActive(false);

        callback?.Invoke();
    }
}
