﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleChangeScene : MonoBehaviour
{
    [SerializeField] private When _when;
    [SerializeField] private Manager_Scenes.SceneName _scene;

    [SerializeField] private UnityEvent _onStartTransition;

    private void Awake()
    {
        if (_when == When.Awake)
            Invoke();
    }

    private void Start()
    {
        if (_when == When.Start)
            Invoke();
    }

    private void Invoke()
    {
        _onStartTransition?.Invoke();
        Manager_Scenes.ChangeScene(_scene);
    }
}
